package IO;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class Reader {

    public static void standartRead(String path) {
        try {
            long startTime = System.currentTimeMillis();
            FileReader fileReader = new FileReader(path);
            while (fileReader.ready()) {
                fileReader.read();
            }
            long elapsedTime = System.currentTimeMillis() - startTime;
            System.out.println("FileReader: File has been read for " + ((elapsedTime / 1000) % 60 / 60) + " minutes, " + ((elapsedTime / 1000) % 60) + " seconds " + ((elapsedTime % 60000) % 60) + " milliseconds");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void difSizeBufferedReader(String path) {
        for (int i = 1, size = 10; i <= 10; i++, size *= 2) {
            bufferedRead(path, size);
        }
    }

    public static void bufferedRead(String path, int size) {
        try {
            long startTime = System.currentTimeMillis();
            BufferedReader bufferedReader = new BufferedReader(new FileReader(path), size);
            while (bufferedReader.ready()) {
                bufferedReader.read();
            }
            long elapsedTime = System.currentTimeMillis() - startTime;
            System.out.println("BufferedReader (size = " + size + " bytes): File has been read for " + ((elapsedTime / 1000) % 60 / 60) + " minutes, " + ((elapsedTime / 1000) % 60) + " seconds " + ((elapsedTime % 60000) % 60) + " milliseconds");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
