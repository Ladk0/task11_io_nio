package IO;

import Transport.Ship;

import java.io.*;

public class DeSerializator {
    public static void serialize(String path, Object object) {
        try {
            FileOutputStream fileOut = new FileOutputStream(path);
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(object);
            out.close();
            fileOut.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Ship deserialize(String path) {
        Ship ship;
        try {
            FileInputStream fileIn = new FileInputStream(path);
            ObjectInputStream in = new ObjectInputStream(fileIn);
            ship = (Ship) in.readObject();
            in.close();
            fileIn.close();
            return ship;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } catch (ClassNotFoundException e) {
            System.out.println("Ship class not found");
            e.printStackTrace();
            return null;
        }
    }
}
