package IO;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class CommentSeeker {

    public static void search(String path) {
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(path));
            while (bufferedReader.ready()) {
                String line = bufferedReader.readLine();
                if (line.contains("//"))
                    System.out.println(line.substring(line.indexOf("//")));
                if (line.contains("/*")) {
                    System.out.println("/*");
                    do {
                        line = bufferedReader.readLine();
                        System.out.println(line.trim());
                    } while (!line.endsWith("*/"));
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
