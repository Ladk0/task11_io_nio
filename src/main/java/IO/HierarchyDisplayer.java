package IO;

import java.io.File;

public class HierarchyDisplayer {
    public static void display(String root, int maxLevel) {
        display(root, 0, maxLevel);
    }

    private static void display(String root, int level, int maxLevel) {
        if (level >= maxLevel)
            return;
        File directory = new File(root);

        File[] fList = directory.listFiles();
        if (fList != null) {
            for (int i = 0; i < fList.length; i++) {
                File file = fList[i];
                if (file.isDirectory()) {
                    System.out.println((getPrefix(level) + file.getName()));
                    display(file.getAbsolutePath(), level + 1, maxLevel);
                } else {
                    System.out.println((getPrefix(level) + file.getName()));
                    display(file.getAbsolutePath(), level, maxLevel);
                }
            }
        }
    }

    private static String getPrefix(int level) {
        String prefix = "";
        for (int i = 0; i < level; i++) {
            prefix = "\t" + prefix;
        }
        return prefix;
    }
}
