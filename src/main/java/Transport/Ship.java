package Transport;

import Droids.Droid;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Ship implements Serializable {
    private ArrayList<Droid> passengers;

    public Ship() {
        passengers = new ArrayList<Droid>();
    }
    
    @Override
    public String toString() {
        return "Ship{" +
                "passengers=" + passengers +
                '}';
    }

    public void put(List<Droid> passengers) {
        this.passengers.addAll(passengers);
    }

    public void put(Droid passenger) {
        this.passengers.add(passenger);
    }

    public Droid get(int index) {
        return passengers.get(index);
    }

    public List<Droid> get(int start, int end) {
        return passengers.subList(start, end);
    }

    public List<Droid> getAll() {
        return passengers;
    }
}
