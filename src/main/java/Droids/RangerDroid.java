package Droids;

public class RangerDroid extends Droid {
    public RangerDroid() {
        health = 75;
        shield = 75;
        damage = 30;
        effectiveHealth = health + shield;
    }
}
