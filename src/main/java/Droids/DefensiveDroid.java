package Droids;

public class DefensiveDroid extends Droid {
    public DefensiveDroid(){
        health = 150;
        shield = 75;
        damage = 20;
        effectiveHealth = health + shield;
    }
}
