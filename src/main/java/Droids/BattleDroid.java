package Droids;

public class BattleDroid extends Droid {
    public BattleDroid() {
        health = 100;
        shield = 50;
        damage = 25;
        effectiveHealth = health + shield;
    }
}
