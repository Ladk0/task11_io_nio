package Droids;

import java.io.Serializable;

public abstract class Droid implements Serializable {
    protected float health;
    protected float shield;
    protected float damage;
    protected transient float effectiveHealth;

    @Override
    public String toString() {
        return "Droid{" +
                "health=" + health +
                ", shield=" + shield +
                ", damage=" + damage +
                ", effectiveHealth=" + effectiveHealth +
                '}';
    }

    public float getHealth() {
        return health;
    }

    public void setHealth(float health) {
        this.health = health;
    }

    public float getShield() {
        return shield;
    }

    public void setShield(float shield) {
        this.shield = shield;
    }

    public float getDamage() {
        return damage;
    }

    public void setDamage(float damage) {
        this.damage = damage;
    }
}
