import Droids.BattleDroid;
import Droids.DefensiveDroid;
import Droids.RangerDroid;
import IO.CommentSeeker;
import IO.DeSerializator;
import IO.HierarchyDisplayer;
import IO.Reader;
import Transport.Ship;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        String fileWithComments = "src/main/resources/CommentsTask.java";
        String file2read = "D:\\file.txt";
        serializeTask();
        Reader.difSizeBufferedReader(file2read);
        Reader.standartRead(file2read);
        CommentSeeker.search(fileWithComments);
        HierarchyDisplayer.display("D:\\Programs\\IntelliJ IDEA 2018.2.3\\Projects\\task11_IO_NIO\\src", 3);
    }

    private static void serializeTask() {
        String path = "src/main/resources/ship.ser";
        BattleDroid battleDroid = new BattleDroid();
        DefensiveDroid defensiveDroid = new DefensiveDroid();
        RangerDroid rangerDroid = new RangerDroid();
        Ship ship = new Ship();
        Ship anotherShip;

        ship.put(Arrays.asList(battleDroid, defensiveDroid, rangerDroid));

        DeSerializator.serialize(path, ship);
        anotherShip = DeSerializator.deserialize(path);

        System.out.println(anotherShip);
    }
}
